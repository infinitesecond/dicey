import RollLexer, {
  integerToken,
  diceSeparatorToken,
  bonusOperatorToken,
} from "../src/parser/RollLexer"

const performTest = (testConfig): void => {
  it(`should lex ${testConfig.input}`, () => {
    // Tokenize
    const result = RollLexer.tokenize(testConfig.input)

    // ensure no errors
    expect(result.errors).toEqual([])

    // Ensure each token matches
    result.tokens.forEach(({ image, tokenType }, index) => {
      const expectedToken = testConfig.tokens[index]

      expect(image).toEqual(expectedToken.value)
      expect(tokenType).toEqual(expectedToken.type)
    })
  })
}

const testConfigurations = [
  {
    input: `5d4`,
    tokens: [
      { value: `5`, type: integerToken },
      { value: `d`, type: diceSeparatorToken },
      { value: `4`, type: integerToken },
    ],
  },
  {
    input: `1d20`,
    tokens: [
      { value: `1`, type: integerToken },
      { value: `d`, type: diceSeparatorToken },
      { value: `20`, type: integerToken },
    ],
  },
  {
    input: `01d10`,
    tokens: [
      { value: `0`, type: integerToken },
      { value: `1`, type: integerToken },
      { value: `d`, type: diceSeparatorToken },
      { value: `10`, type: integerToken },
    ],
  },
  {
    input: `0d2`,
    tokens: [
      { value: `0`, type: integerToken },
      { value: `d`, type: diceSeparatorToken },
      { value: `2`, type: integerToken },
    ],
  },
  {
    input: `1d8 + 3`,
    tokens: [
      { value: `1`, type: integerToken },
      { value: `d`, type: diceSeparatorToken },
      { value: `8`, type: integerToken },
      { value: `+`, type: bonusOperatorToken },
      { value: `3`, type: integerToken },
    ],
  },
  {
    input: `1d20 - 5`,
    tokens: [
      { value: `1`, type: integerToken },
      { value: `d`, type: diceSeparatorToken },
      { value: `20`, type: integerToken },
      { value: `-`, type: bonusOperatorToken },
      { value: `5`, type: integerToken },
    ],
  },
]
const invalidInputs = [`1.5d20`, `1d6x4`, `jimmy`]

describe(`RollLexer`, () => {
  testConfigurations.forEach(testConfig => performTest(testConfig))
  invalidInputs.forEach(invalidInput => {
    it(`should error on ${invalidInput}`, () => {
      const result = RollLexer.tokenize(invalidInput)
      expect(result.errors.length).toBeGreaterThan(0)
    })
  })
})
