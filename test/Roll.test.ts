import Roll from "../src/Roll"

describe(`Roll`, () => {
  describe(`constructor`, () => {
    it(`should create 1d8`, () => {
      const roll = new Roll(1, 8)
      expect(roll.numRolls).toEqual(1)
      expect(roll.sides).toEqual(8)
      expect(roll.bonus).toEqual(0)
    })

    it(`should create 1d20+5`, () => {
      const roll = new Roll(1, 20, 5)
      expect(roll.numRolls).toEqual(1)
      expect(roll.sides).toEqual(20)
      expect(roll.bonus).toEqual(5)
    })
  })

  describe(`validateSides`, () => {
    it(`should return with valid input`, () => {
      const rolls = [new Roll(1, 8), new Roll(1, 20, 5), new Roll(1, 1)]

      rolls.forEach(roll => {
        const func = (): void => roll.validateSides()
        expect(func).not.toThrow()
      })
    })

    it(`should throw error if sides are less than 1`, () => {
      const rolls = [new Roll(1, 0), new Roll(1, -1), new Roll(1, -5)]
      rolls.forEach(roll => {
        const func = (): void => roll.validateSides()
        expect(func).toThrowError()
      })
    })

    it(`should throw error if sides are not integer`, () => {
      const rolls = [new Roll(1, 1.5), new Roll(1, 0.3), new Roll(1, NaN)]
      rolls.forEach(roll => {
        const func = (): void => roll.validateSides()
        expect(func).toThrowError()
      })
    })
  })

  describe(`validateNumRolls`, () => {
    it(`should return with valid input`, () => {
      const rolls = [new Roll(1, 8), new Roll(56, 20, 5), new Roll(2, 1)]

      rolls.forEach(roll => {
        const func = (): void => roll.validateNumRolls()
        expect(func).not.toThrow()
      })
    })

    it(`should throw error if numRolls are less than 1`, () => {
      const rolls = [new Roll(0, 1), new Roll(-1, 1), new Roll(-20, 5)]
      rolls.forEach(roll => {
        const func = (): void => roll.validateNumRolls()
        expect(func).toThrowError()
      })
    })

    it(`should throw error if numRolls are not integer`, () => {
      const rolls = [new Roll(1.5, 3), new Roll(3 / 5, 2), new Roll(NaN, 4)]
      rolls.forEach(roll => {
        const func = (): void => roll.validateNumRolls()
        expect(func).toThrowError()
      })
    })
  })

  describe(`validateBonus`, () => {
    it(`should return with valid input`, () => {
      const rolls = [new Roll(1, 8), new Roll(56, 20, 5), new Roll(2, 1, -5)]

      rolls.forEach(roll => {
        const func = (): void => roll.validateBonus()
        expect(func).not.toThrow()
      })
    })

    it(`should throw error if bonus is not integer`, () => {
      const rolls = [
        new Roll(1, 3, 1.5),
        new Roll(1, 4, 2 / 3),
        new Roll(1, 2, NaN),
      ]
      rolls.forEach(roll => {
        const func = (): void => roll.validateBonus()
        expect(func).toThrowError()
      })
    })
  })

  describe(`validate`, () => {
    it(`should return successfully`, () => {
      const rolls = [new Roll(1, 8), new Roll(56, 20, 5), new Roll(2, 1, -5)]

      rolls.forEach(roll => {
        const validate = (): void => roll.validate()
        expect(validate).not.toThrow()
      })
    })

    it(`should throw error`, () => {
      const rolls = [
        new Roll(-1, 8, 5),
        new Roll(56, -20, 5),
        new Roll(2, 1, 0.5),
      ]

      rolls.forEach(roll => {
        const validate = (): void => roll.validate()
        expect(validate).toThrowError()
      })
    })
  })

  describe(`isValid`, () => {
    it(`should return valid`, () => {
      const rolls = [new Roll(1, 8), new Roll(56, 20, 5), new Roll(2, 1, -5)]

      rolls.forEach(roll => {
        expect(roll.isValid).toBe(true)
      })
    })

    it(`should be invalid because of numRolls`, () => {
      const rolls = [
        new Roll(-1, 3, 1),
        new Roll(1.5, 4, 2),
        new Roll(NaN, 2, 4),
      ]
      rolls.forEach(roll => {
        expect(roll.isValid).toBe(false)
      })
    })

    it(`should be invalid because of sides`, () => {
      const rolls = [
        new Roll(1, -3, 1),
        new Roll(4, 8.2, 2),
        new Roll(2, NaN, 4),
      ]
      rolls.forEach(roll => {
        expect(roll.isValid).toBe(false)
      })
    })

    it(`should be invalid because of numBonus`, () => {
      const rolls = [new Roll(-1, 3, 1.5), new Roll(3, 4, NaN)]
      rolls.forEach(roll => {
        expect(roll.isValid).toBe(false)
      })
    })
  })

  describe(`toObject`, () => {
    it(`should return expected object`, () => {
      const tests = [
        {
          roll: new Roll(1, 8),
          expected: { numRolls: 1, sides: 8, bonus: 0 },
        },
        {
          roll: new Roll(56, 20, 5),
          expected: { numRolls: 56, sides: 20, bonus: 5 },
        },
        {
          roll: new Roll(2, 1, -5),
          expected: { numRolls: 2, sides: 1, bonus: -5 },
        },
      ]

      tests.forEach(({ roll, expected }) => {
        expect(roll.toObject()).toEqual(expected)
      })
    })
  })

  describe(`roll`, () => {
    it(`should return totals within an expected range`, () => {
      const tests = [
        {
          roll: new Roll(1, 8),
          lowerLimit: 1,
          upperLimit: 8,
        },
        {
          roll: new Roll(56, 20, 5),
          lowerLimit: 61,
          upperLimit: 1125,
        },
        {
          roll: new Roll(2, 4, -5),
          lowerLimit: -3,
          upperLimit: 3,
        },
      ]

      tests.forEach(({ roll, lowerLimit, upperLimit }) => {
        for (let i = 0; i < 500; i++) {
          const result = roll.roll()
          expect(result).toBeGreaterThanOrEqual(lowerLimit)
          expect(result).toBeLessThanOrEqual(upperLimit)
        }
      })
    })
  })

  describe(`rollVerbose`, () => {
    it(`should return results within an expected range with correct bonus`, () => {
      ;[new Roll(1, 8), new Roll(56, 20, 5), new Roll(2, 4, -5)].forEach(
        roll => {
          for (let i = 0; i < 200; i++) {
            // Perform roll
            const { rollResults, bonus } = roll.rollVerbose()

            // Ensure each result is within limits
            rollResults.forEach(result => {
              expect(result).toBeGreaterThanOrEqual(1)
              expect(result).toBeLessThanOrEqual(roll.sides)
            })

            // Check bonus
            expect(bonus).toBe(roll.bonus)
          }
        },
      )
    })
  })

  describe(`utilities`, () => {
    it(`should serialize`, () => {
      ;[new Roll(1, 8), new Roll(56, 20, 5), new Roll(2, 4, -5)].forEach(
        roll => {
          expect(JSON.stringify(roll)).toEqual(JSON.stringify(roll.toJSON()))
        },
      )
    })

    it(`should create from an object`, () => {
      const tests = [
        { numRolls: 1, sides: 8, bonus: 0 },
        { numRolls: 56, sides: 20, bonus: 5 },
        { numRolls: 2, sides: 1, bonus: -5 },
        { numRolls: 3, sides: 6, bonus: -22 },
        { numRolls: 4, sides: 36, bonus: 0 },
      ]

      tests.forEach(testConfig => {
        const roll = Roll.fromObject(testConfig)
        expect(roll.numRolls).toEqual(testConfig.numRolls)
        expect(roll.sides).toEqual(testConfig.sides)
        expect(roll.bonus).toEqual(testConfig.bonus)
      })
    })

    it(`toString() should return standard dice notation`, () => {
      ;[
        { expected: `1d8`, roll: new Roll(1, 8, 0) },
        { expected: `1d8`, roll: new Roll(1, 8) },
        { expected: `1d8`, roll: new Roll(1, 8, -0) },
        { expected: `1d8+1`, roll: new Roll(1, 8, 1) },
        { expected: `52d20-12`, roll: new Roll(52, 20, -12) },
        { expected: `1d20-5`, roll: new Roll(1, 20, -5) },
      ].forEach(({ expected, roll }) => {
        expect(roll.toString()).toEqual(expected)
      })
    })
  })

  describe(`parse`, () => {
    it(`should correctly parse`, () => {
      const testConfigurations = [
        { input: `1d8`, expected: { numRolls: 1, sides: 8, bonus: 0 } },

        {
          input: `56 d 20 + 5`,
          expected: { numRolls: 56, sides: 20, bonus: 5 },
        },
        { input: `2d1 - 5`, expected: { numRolls: 2, sides: 1, bonus: -5 } },
        { input: `3 d6 -22`, expected: { numRolls: 3, sides: 6, bonus: -22 } },
        { input: `4d36-0`, expected: { numRolls: 4, sides: 36, bonus: -0 } },
      ]
      testConfigurations.forEach(
        ({ input, expected: { numRolls, sides, bonus } }) => {
          const roll = Roll.parse(input)
          expect(roll.numRolls).toEqual(numRolls)
          expect(roll.sides).toEqual(sides)
          expect(roll.bonus).toEqual(bonus)
        },
      )
    })

    it(`should throw error`, () => {
      const testConfigurations = [`jimmy`, `1ad3+3`, `1d6x4`, `1d6+4 + 5 - 6`]
      testConfigurations.forEach(input => {
        const parseAttempt = (): void => {
          Roll.parse(input)
        }
        expect(parseAttempt).toThrowError()
      })
    })
  })
})
