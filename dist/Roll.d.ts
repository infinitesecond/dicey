/**
 * A detailed result of a dice roll
 * @property {number[]} rollResults - results of rolls
 * @property {number} bonus - bonus applied
 * @property {number} total - total of roll results and bonus
 */
interface Result {
    rollResults: Array<number>;
    bonus: number;
    total: number;
}
/**
 * A POJO representing a dice roll
 * @property {integer} numRolls
 * @property {integer} sides
 * @property {integer} bonus
 */
interface RollObject {
    numRolls: number;
    sides: number;
    bonus: number;
}
/**
 * Class representing a dice roll
 * @property {integer} numRolls - Number of times the dice should be rolled.  Should be greater than 0.
 * @property {integer} sides    - Number of sides of the dice. Should be greater than 0.
 * @property {integer} bonus    - Bonus that should applied to a dice roll.
 */
declare class Roll {
    private _numRolls;
    private _sides;
    private _bonus;
    constructor(numRolls: number, sides: number, bonus?: number);
    static parse(input: string): Roll;
    static fromObject(roll: RollObject): Roll;
    /** Accessor for number of times the dice should be rolled */
    /** Setter for number of times the dice should be rolled */
    numRolls: number;
    /**
     * Validates whether the number of rolls is valid
     * @throws if `numRolls` is not an integer
     * @throws if `numRolls` is less than or equal to 0
     */
    validateNumRolls(): void;
    /** Accessor for number of sides on a dice */
    /** Setter for number of sides on a dice */
    sides: number;
    /**
     * Validates whether the number of sides is valid
     * @throws if `sides` is not an integer
     * @throws if `sides` is less than or equal to 0
     */
    validateSides(): void;
    /** Accessor for the bonus that should be added to a dice roll */
    /** Setter for the bonus that should be added to a dice roll */
    bonus: number;
    /**
     * Validates whether the bonus is valid
     * @throws if bonus is not an integer
     */
    validateBonus(): void;
    /** Validates roll and throws error if invalid */
    validate(): void;
    /** Returns whether or not this roll is valid */
    readonly isValid: boolean;
    /** Object representation of a roll */
    toObject(): object;
    /** JSON serialization */
    toJSON(): object;
    /**
     * Roll the dice
     * @returns {number} result of dice roll
     * @throws error if roll is invalid
     */
    roll(): number;
    /**
     * Roll the dice and return a verbose set of results
     * @returns {Result} representation of dice roll
     * @throws error if roll is invalid
     */
    rollVerbose(): Result;
}
export default Roll;
