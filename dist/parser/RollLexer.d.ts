import { Lexer } from "chevrotain";
export declare const whiteSpaceToken: import("chevrotain").TokenType;
export declare const integerToken: import("chevrotain").TokenType;
export declare const diceSeparatorToken: import("chevrotain").TokenType;
export declare const bonusOperatorToken: import("chevrotain").TokenType;
export declare const tokens: import("chevrotain").TokenType[];
declare const RollLexer: Lexer;
export default RollLexer;
