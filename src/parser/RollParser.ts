import { EmbeddedActionsParser } from "chevrotain"
import {
  integerToken,
  diceSeparatorToken,
  bonusOperatorToken,
  tokens,
} from "./RollLexer"

class RollParser extends EmbeddedActionsParser {
  constructor() {
    super(tokens)

    this.performSelfAnalysis()
  }
  public roll = this.RULE(`roll`, () => {
    const diceRoll = this.SUBRULE(this.diceRoll)

    const bonus = this.OPTION(() => {
      return this.SUBRULE(this.bonus)
    }) || { bonus: 0 }
    return Object.assign({}, diceRoll, bonus)
  })

  public diceRoll = this.RULE(`diceRoll`, () => {
    const numRollsString = this.CONSUME(integerToken).image
    this.CONSUME(diceSeparatorToken)
    const sidesString = this.CONSUME1(integerToken).image

    const numRolls = parseInt(numRollsString)
    const sides = parseInt(sidesString)

    return { numRolls, sides }
  })

  public bonus = this.RULE(`bonus`, () => {
    const operatorString = this.CONSUME(bonusOperatorToken).image
    const bonusAmountString = this.CONSUME(integerToken).image

    const bonusModifier = operatorString === `-` ? -1 : 1
    const bonusAmount = parseInt(bonusAmountString)

    return { bonus: bonusModifier * bonusAmount }
  })
}

export default RollParser
